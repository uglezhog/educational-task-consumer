package ru.bell.consumer.service.broker;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import ru.bell.consumer.mapper.EmployeeMapper;
import ru.bell.consumer.repository.EmployeeRepository;
import ru.bell.mainlib.dto.EmployeeDto;

import java.util.List;

@Slf4j
@Service
@EnableKafka
@KafkaListener(
		topics = "${kafka.topic.toMainEducationalTopic}",
		groupId = "${spring.kafka.consumer.group-id}")
@RequiredArgsConstructor
public class EmployeeBrokerListener {

	private final EmployeeRepository employeeRepository;
	private final EmployeeMapper employeeMapper;
	private final ObjectMapper objectMapper;

	@KafkaHandler
	public void consume(EmployeeDto employeeDto) {
		log.info(String.format("#### -> Consumed message -> %s", employeeDto));
		employeeRepository.save(employeeMapper.toEntity(employeeDto));
	}

	@KafkaHandler
	public void consume(List<EmployeeDto> dtos) {
		log.info(String.format("#### -> Consumed message -> %s", dtos));
		List<EmployeeDto> employeeDtos = objectMapper.convertValue(dtos, new TypeReference<>() {});
		var employees = employeeMapper.toEntities(employeeDtos);
		var newDto = employeeMapper.toDtos(employees);
		employeeRepository.saveAll(employees);
	}
}