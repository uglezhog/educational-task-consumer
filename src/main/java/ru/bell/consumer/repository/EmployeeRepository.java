package ru.bell.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bell.consumer.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}