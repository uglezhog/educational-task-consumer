package ru.bell.consumer.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import ru.bell.consumer.entity.Employee;
import ru.bell.mainlib.dto.EmployeeDto;

import java.util.List;

/**
 * TODO Class Description
 *
 * @author Alexander Maximov
 * @since 20.04.2022
 */
@Mapper(componentModel = "spring")
public abstract class EmployeeMapper {

	@Mapping(target = "fullName", source = "employee", qualifiedByName = "fullName")
	public abstract EmployeeDto toDtos(Employee employee);

	public abstract List<EmployeeDto> toDtos(List<Employee> employees);

	@Named("fullName")
	String fullName(Employee employee) {
		return String.format("%s %s %s", employee.getFirstName(), employee.getLastName(), employee.getMiddleName());
	}

	public abstract Employee toEntity(EmployeeDto dto);

	public abstract List<Employee> toEntities(List<EmployeeDto> dto);

	@AfterMapping
	void afterMapToEntity(@MappingTarget Employee employee, EmployeeDto dto) {
		var names = dto.getFullName().split(" ");
		employee.setFirstName(names[0]);
		employee.setLastName(names[1]);
		employee.setMiddleName(names[2]);
	}
}
